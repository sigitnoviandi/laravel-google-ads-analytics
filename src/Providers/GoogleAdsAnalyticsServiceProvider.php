<?php

namespace SigitNoviandi\GoogleAdsAnalytics\Providers;

use SigitNoviandi\GoogleAdsAnalytics\Console\RefreshTokenCommand;
use SigitNoviandi\GoogleAdsAnalytics\GoogleAds;
use Illuminate\Support\ServiceProvider;
use SigitNoviandi\GoogleAdsAnalytics\Analytics;
use SigitNoviandi\GoogleAdsAnalytics\AnalyticsClient;
use SigitNoviandi\GoogleAdsAnalytics\AnalyticsClientFactory;
use SigitNoviandi\GoogleAdsAnalytics\Exceptions\Analytics\InvalidConfiguration;

class GoogleAdsAnalyticsServiceProvider extends ServiceProvider
{
    
    protected $config_file = 'google-ads-analytics';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
       
        $config_path = function_exists('config_path') ? config_path($this->config_file . '.php') : $this->config_file . '.php';

        $this->publishes([
            __DIR__.'/../Config/config.php' => $config_path,
        ], 'config');
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Console commands
        $this->commands([
            RefreshTokenCommand::class,
        ]);
        
        $this->register_google_ads();
        
        $this->register_google_analytics_client();
        
        $this->register_google_analytics();
    }
    
    protected function register_google_ads(){
        $this->app->bind(GoogleAds::class, function ($app) {
            return new GoogleAds();
        });
    }
    
    protected function register_google_analytics_client(){
        $this->app->bind(AnalyticsClient::class, function () {
            $analyticsConfig = config($this->config_file);

            return AnalyticsClientFactory::createForConfig($analyticsConfig);
        });
    }
    
    protected function register_google_analytics(){
        $this->app->bind(Analytics::class, function () {
            $analyticsConfig = config($this->config_file);
            $this->guardAgainstInvalidConfiguration($analyticsConfig);

            $client = app(AnalyticsClient::class);
            
            return new Analytics($client, $analyticsConfig['view_id']);
        });

    }
    
    protected function guardAgainstInvalidConfiguration(array $analyticsConfig = null)
    {
        if (empty($analyticsConfig['view_id'])) {
            throw InvalidConfiguration::viewIdNotSpecified();
        }

        if (is_array($analyticsConfig['service_account_credentials_json'])) {
            return;
        }

        if (! file_exists($analyticsConfig['service_account_credentials_json'])) {
            throw InvalidConfiguration::credentialsJsonDoesNotExist($analyticsConfig['service_account_credentials_json']);
        }
    }
}
