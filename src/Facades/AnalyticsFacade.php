<?php

namespace SigitNoviandi\GoogleAdsAnalytics\Facades;

use SigitNoviandi\GoogleAdsAnalytics\Analytics as AliasGoogleAnalytics;
use Illuminate\Support\Facades\Facade;

/**
 * @see \SigitNoviandi\GoogleAdsAnalytics\Analytics
 */
class AnalyticsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AliasGoogleAnalytics::class;
    }
}
